"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    
    if 0 < control_dist_km <= 200:
        time = find_time(control_dist_km, 34)
    
    elif 200 < control_dist_km <= 400:
        time = find_time(200, 34) + find_time(control_dist_km - 200, 32)
    
    elif 400 < control_dist_km <= 600:
        time = find_time(200, 34) + find_time(200, 32) + find_time(control_dist_km - 400, 30)
    
    elif 600 < control_dist_km <= 1000:
        time = find_time(200, 34) + find_time(200, 32) + find_time(200, 30) + find_time(control_dist_km - 600, 28)
    
    elif 1000 < control_dist_km <= 1300:
        time = find_time(200, 34) + find_time(200, 32) + find_time(200, 30) + find_time(400, 28) + find_time(control_dist_km - 1000, 26)
    else:
        print("If you can see this, something has gone wrong!")
        return False
    
    return add_time(brevet_start_time, time)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if 0 < control_dist_km <= 200:
        time = find_time(control_dist_km, 15)
    
    elif 200 < control_dist_km <= 400:
        time = find_time(200, 15) + find_time(control_dist_km - 200, 15)
    
    elif 400 < control_dist_km <= 600:
        time = find_time(200, 15) + find_time(200, 15) + find_time(control_dist_km - 400, 15)
    
    elif 600 < control_dist_km <= 1000:
        time = find_time(200, 15) + find_time(200, 15) + find_time(200, 15) + find_time(control_dist_km - 600, 11.428)
    
    elif 1000 < control_dist_km <= 1300:
        time = find_time(200, 15) + find_time(200, 15) + find_time(200, 15) + find_time(400, 11.428) + find_time(control_dist_km - 1000, 13.333)
    else:
        print("If you can see this, something has gone wrong!")
        return False
    
    return add_time(brevet_start_time, time)

#This returns a number of hours, to be added to the start time in either open_time or close_time
def find_time(km, spd):
    hrs = km/spd
    return hrs

#This is supposed to add the result from find_time to the start time, meant to be called from within open_time or close_time
def add_time(start_time, added_time):
    #first lets figure out how many hours to add
    hours = int((added_time // 1) // 24)
    #second lets figure out how many minutes to add
    minutes = int((added_time % 1) * 60)
    #also, in case the brevet goes on for days
    days = 0
    if hours >= 24:
        days = int(hours // 24)
        hours = hours % 24

    #Now we can do some stuff to add hours and minutes to the start_time, and return open_time or close_time we need
    min_substring = int(start_time[15:16]) + minutes
    hr_substring = int(start_time[12:13]) + hours
    days_substring = int(start_time[9:10]) + days
    if minutes <= 9:
        min_substring = "0" + str(min_substring)
    if hours <= 9:
        hr_substring = "0" + str(hr_substring)
    if days <= 9:
        days_substring = "0" + str(days_substring)

    new_string = start_time[0:8] + str(days_substring) + start_time[10:11] + str(hr_substring) + start_time[13:14] + str(min_substring) + start_time[16:]

    return new_string