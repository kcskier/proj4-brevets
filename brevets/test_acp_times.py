from acp_times import open_time, close_time

import nose

default_date = "2017-01-01T00:00:00"
default_dist = 200
default_control_dist = 50

def test_zero_open():
	"""Tests for different distances inputs"""
	open_time(0,0,0) == "False"
	
def test_zero_close():
	"""Tests for different distances inputs"""
	close_time(0,0,0) == "False"

def test_defaults_open():
	"""Runs a test on open_time with default values"""
	open_time(default_control_dist, default_dist, default_date) == default_date
	
def test_defaults_close():
	"""Runs a test on close_time with default values"""
	close_time(default_control_dist, default_dist, default_date) == default_date
	
def test_big_numbers_open():
	"""Runs a test with large numbers"""
	for i in range(0, 1500):
		open_time(50, i, default_date) == "2017-01-01T00:00:00"
		
def test_big_numbers_close():
	"""Runs a test with large numbers"""
	for i in range(0, 1500):
		close_time(50, i, default_date) == "2017-01-01T00:00:00"

def test_big_ctrl_dist_open():
	"""Runs all the possible (+ a few more) numbers through control_dist"""
	for i in range(100,2000):
		if open_time(i, 1300, default_date) == "2017-01-01T00:00:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T01:00:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T01:45:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T02:30:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T02:15:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T02:15:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T03:00:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T03:30:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T03:30:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T03:00:00":
			return
		elif open_time(i, 1300, default_date) == "2017-01-01T04:30:00":
			return
		else:
			open_time(i, 1300, default_date) == False
		i = i +100

def test_big_ctrl_dist_close():
	"""Runs all the possible (+ a few more) numbers through control_dist"""
	for i in range(100,2000):
		if close_time(i, 1300, default_date) == "2017-01-01T00:00:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T01:00:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T01:45:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T02:30:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T02:15:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T02:15:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T03:00:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T03:30:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T03:30:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T03:00:00":
			return
		elif close_time(i, 1300, default_date) == "2017-01-01T04:30:00":
			return
		else:
			close_time(i, 1300, default_date) == False
		i = i +100
		
def test_bad_date_open():
	"""Providing a bad date"""
	try:
		open_time(default_control_dist, default_dist, "0") == False
	except:
		pass
	
def test_bad_date_close():
	"""Providing a bad date"""
	try:
		close_time(default_control_dist, default_dist, "0") == False
	except:
		pass

