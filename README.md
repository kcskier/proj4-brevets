# Project 4: Brevet time calculator with Ajax

Remaking the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Modified and updated by Kendell Crocker kendellc@uoregon.edu

## ACP controle times

Yes, I did fact check, it actually is "controle" with an 'e'. Controls are points where a rider must obtain proof of passage, and control(e) times are the minimum and maximum times by which the rider must arrive at the particular control point.   

You can find a discription of the algorithm here https://rusa.org/pages/acp-brevet-control-times-calculator and here https://rusa.org/pages/rulesForRiders

Click here for the official calculator https://rusa.org/octime_acp.html

## To install

*Get Docker. If you are running linux or mac os, run these commands:

```
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```

*If you are on windows, follow the link and download the .msi:

https://download.docker.com/win/static/stable/x86_64/docker-17.09.0-ce.zip

PLEASE NOTE THAT DOCKER ON WINDOWS DOES NOT ALWAYS WORK CORRECTLY. IT IS NOT RECOMMENDED THAT YOU RUN THIS ON WINDOWS.

*Also we need git. For linux or mac:

```
sudo apt-get install git
```

*For windows users:

https://git-scm.com/download/win

*navigate to the folder with the Dockerfile

```
/path/to/repo/brevets
```

*Run the "run.sh" script in a terminal

```
./run.sh
```

*And thats it! Open a browser and navigate to localhost:<portnumber> (Default is 5000) and you are ready to go!




## Testing

Included is a set of python nosetests. To run them:

*Make sure you have nose installed. Run this command in a terminal to install:

```
sudo pip install nose
```

## What is Here

Everything you need to get this setup and running. Well, everything except docker. Get that before trying to run the start script. Include in this is:

* The working application.

* An automated 'nose' test suite.

* Dockerfile

